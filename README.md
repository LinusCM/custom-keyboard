# Custom keyboard
This is my custom keyboard build based off the [ZSA Planck EZ](https://www.zsa.io/planck).

In this repository you can find the custom case i've designed from the bottom,
as well as my personal layout firmware based off the colemak-dh layout.

![Keyboard](resources/keyboard.jpg)


## Hardware & software
A list of the software & hardware used during development with relevant links.

#### Printer & materials
- [Creality Ender 3 S1 Pro](https://www.creality.com/products/creality-ender-3-s1-pro-fdm-3d-printer)
- [FormFutura rPLA 1.75mm](https://formfutura.com/product/reform-rpla/)

#### Keys & caps
- [KBDfans BoW](https://kbdfans.com/products/pbtfans-doubleshot-bow?_pos=2&_sid=ba8d39cde&_ss=r)
- [Gateron Milky Yellow](https://www.gateron.co/products/gateron-ks-3-milky-pro-switch-set?_pos=1&_psq=milky+y&_ss=e&_v=1.0&variant=40124530098265)

#### Software
- [Fusion 360 Personal](https://www.autodesk.com/products/fusion-360/personal)
- [Wally](https://www.zsa.io/wally/)
- [Oryx configurator](https://configure.zsa.io/)


## Print settings
The case was printed with the Creality Ender 3 S1 Pro,
with the upper and lower split in half to fit on the printbed.

It's recommended to print each part seperately to avoid warping with said printer.
Using glue greately improved the end result.

#### Time to print
- Top halves: 2.5 hrs
- Bottom halves: 4.5 hrs

#### Print settings
- 60mm/s
- 40% infill
- 0.2mm resolution
- Regular PLA temps
- No supports

## Assembly
Superglue has been used to glue each half together, then the
ZSA pcb has been screwed in with with regular M3x5 motherboard screws.

Rest is friction fit.

8.5mm diameter rubber feet has been used at the bottom.
